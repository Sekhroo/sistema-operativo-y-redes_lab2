#ifndef FORK_H // previnir inclusiones de multiples archivos header
#define FORK_H

using namespace std;
#include <iostream>

// Declarar clase Fork
class Fork {

    private:
        char *url; // Variable para guardar la URL del video

        pid_t pid; // Variable para crear el hijo

    public:
        // Constructor base
        Fork(char *url);
        
        // Creación de un proceso hijo
        void crear_subproceso();

        // Ejecutar codigos
        void ejecutar();
};

#endif