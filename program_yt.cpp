#include <iostream>
using namespace std;

#include "Fork.h"

// Main
int main (int argc, char **argv) {
    char *url_yt = argv[1];

    Fork miFork(url_yt);

    return 0;
}