using namespace std;
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>

#include "Fork.h"

Fork::Fork (char *url) {
  this->url = url;
  crear_subproceso();
  ejecutar();
}
    
// ======== Métodos =========
// Crear subproceso (hijo)
void Fork::crear_subproceso() {
  // Crea el proceso hijo.
  pid = fork();
}
    
void Fork::ejecutar() {
  char *url = this->url;

  if (pid < 0) { // Valida la creación de proceso hijo.
    cout << "No se pudo crear el proceso ..." << endl;
        
  } else if (pid == 0) { // Código del proceso hijo.
    cout << "ID Proceso hijo: " << getpid() << endl;
    cout << "Ejecuta código proceso hijo ..." << endl;

    // Coloca parte del comando en un arreglo
    // Comando: descargar audio en formato mp3 de un video de youtube

    // Comando anterior a resolver dudas en clase:
    //char* param[] = {"youtube-dl","--extract-audio","--audio-format","mp3",url,"-o","%(title)s.%(ext)s", NULL};
    
    // Solución propuesta posterior a clase: (cambio de nombre)
    char* param[] = {"youtube-dl","-x","--audio-format","mp3",url,"-o","song.mp3", NULL};


    // Ejecutar el comando de youtube-dl más el arreglo
    execvp ("youtube-dl", param);

    // Comando completo: youtube-dl --extract-audio --audio-format mp3 https://youtu.be/gTq0MuknEuc -o song.mp3
    // Video de prueba: https://youtu.be/gTq0MuknEuc

    // Espera un segundo para continuar (para pruebas).
    sleep(1);
        
  } else { // Código proceso padre.

    // Padre espera por el término del proceso hijo.
    wait (NULL);

    cout << "Termina código de proceso hijo ..." << endl;

    cout << "Continua con código proceso padre: " << getpid() << endl;


    // Coloca parte del comando en un arreglo
    // Comando: abrir el audio del video descargado
    cout << "Ejecutar audio descargado via VLC ...: " << endl;

    // Arreglo de comandos
    // Solución pre-dudas en clase:
    //char* param[] = {"vlc","'Popcat but 4K and 60fps.mp3'", NULL};

    // Solución propuesta despues de resolver dudas en clase: (en proceso hijo nuevo)
    char* param[] = {"mplayer","song.mp3", NULL};

    // Ejecutar el audio del video descargado
    execvp ("mplayer", param);


    // Comando completo: vlc 'Popcat but 4K and 60fps.mp3'
    // Nombre del video de prueba: Popcat but 4K and 60fps.mp3

    //Termina proceso padre
    cout << "Termina proceso padre..." << endl;
  }
}